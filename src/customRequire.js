const fs = require('fs');
const babel = require('@babel/parser');
const traverse = require('@babel/traverse').default;
const getValueFromAST = require("./helpers/getValueFromAST");
const generate = require('@babel/generator').default;


const customRequire = (path) => {
    const content = fs.readFileSync(path,'utf-8');
    const ast = babel.parse(content);
    let imported = null;
    const identifiers = {}


    traverse(ast,{
        FunctionDeclaration({ node }) {
            identifiers[node.id.name] = getValueFromAST(node.body,identifiers,true)
        },
        VariableDeclarator({ node }) {
            identifiers[node.id.name] = getValueFromAST(node.init,identifiers)
        },
        AssignmentExpression({ node : { left, right } }) {
            if(left.object.name === 'module' && left.property.name === 'customExports') {
                imported = getValueFromAST(right,identifiers);
            }
        }
    })

    return imported;
}

module.exports = customRequire;