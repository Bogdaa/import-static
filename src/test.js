const a = 3;

const b = `string ${a}`;

const boolean = true;

const simpleObject = {
    name:'Bogdan',
    surname:'Nikituk'
}


const array = [simpleObject,2,3,4]

const undef = undefined;

const nul = null;

function func() {
    console.log(array)
}


module.customExports = {
    c:1,
    string:`${b} aaaa`,
    b:array,
    nul,
    undef,
    func,
}