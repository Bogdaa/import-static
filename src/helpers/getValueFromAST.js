const generate = require('@babel/generator').default;

const getValueFromAST = (ast,params,isDefaultFunction = false) => {
    const paramsNames = Object.keys(params).reduce((acc,current,index) => {
        return index === 0 ? current : `${acc},${current}`
    },'')

    if(isDefaultFunction) {
        return eval(`({ ${paramsNames} }) => function () ${generate(ast).code}`)(params)
    }

    return eval(`({ ${paramsNames} }) => (${generate(ast).code})`)(params)
}

module.exports = getValueFromAST